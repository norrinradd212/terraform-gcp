region = "europe-west1"

zone = "europe-west1-b"

image = "ubuntu-os-cloud/ubuntu-2004-lts"

machine_type = "e2-micro"

subnetCIDRblock = "10.0.0.0/16"

CIDRblock = "0.0.0.0/0"

projectName = "testoleg"

# resource "google_compute_instance" "my-server" {
#   name         = "${var.projectName}-gcp-server"
#   machine_type = var.machine_type
#   boot_disk {
#     initialize_params {
#       image = var.image
#     }
#   }
#   network_interface {
#     subnetwork = google_compute_subnetwork.test_network_subnetwork.name
#     access_config {

#     }
#   }
#   metadata = {
#     ssh-keys = "${file("~/id_rsa.pub")}"
#   }
#   metadata_startup_script = file("./user_data.sh")

# }

#-----------------------------------------------------------
module "test_inst" {
  source         = "git::https://gitlab.com/norrinradd212/terraform-modules.git//gcp_vm_instance"
  ssh-key       = "~/id_rsa.pub"
  startup-script = "./user_data.sh"
  region         = var.region
  projectName    = var.projectName
}



#-----------------------------------------------------------

resource "google_compute_firewall" "default" {
  name    = "${var.projectName}-firewall"
  network = module.test_inst.network_name
  allow {
    protocol = "icmp"
  }
  dynamic "allow" {
    for_each = ["80", "443", "22"]
    content {
      protocol = "tcp"
      ports    = [allow.value]
    }
  }

  #   allow {
  #     protocol = "tcp"
  #     ports    = ["22"]
  #   }
  #   allow {
  #     protocol = "tcp"
  #     ports    = ["80", "8080", "1000-2000"]
  #   }

  source_tags   = ["web"]
  source_ranges = [var.CIDRblock]
}

#-----------------------------------------------------------

# resource "google_compute_network" "test_network" {
#   name = "${var.projectName}-network"
# }
# #-----------------------------------------------------------

# resource "google_compute_subnetwork" "test_network_subnetwork" {
#   name          = "${var.projectName}-subnetwork"
#   region        = var.region
#   network       = google_compute_network.test_network.self_link
#   ip_cidr_range = var.subnetCIDRblock
# }
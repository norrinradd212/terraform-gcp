terraform {
  backend "gcs" {
    credentials = "./cred.json"
    bucket      = "test-terraform1331"
  }
}

provider "google" {
  credentials = file("./cred.json")
  project     = "project-for-test-345407"
  region      = var.region
  zone        = var.zone

}
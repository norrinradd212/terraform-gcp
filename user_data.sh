#!/bin/bash

apt update && apt upgrade
apt install python3 python3-pip -y
pip3 install flask


myip=`curl -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip`

cat <<EOF > /home/olehhordon/start_scr.py
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello, I use Flask and this is my ip $myip!'

app.run(host='0.0.0.0', port=80)
EOF

python3 /home/olehhordon/start_scr.py